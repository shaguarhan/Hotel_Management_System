# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/shaguar/project/hotel/Hotel_Management_System/sqlite/sqlite3.c" "/home/shaguar/project/hotel/Hotel_Management_System/build/CMakeFiles/sqlite_lib.dir/sqlite/sqlite3.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../EventHandler"
  "../com"
  "../protocol"
  "../frame"
  "../FrameHandler"
  "../sqlite"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
